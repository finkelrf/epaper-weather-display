#ifndef DISPLAY_H
#define DISPLAY_H

#define LILYGO_T5_V213
#include <boards.h>
#include <GxEPD.h>
#include <GxDEPG0213BN/GxDEPG0213BN.h>
#include <GxIO/GxIO_SPI/GxIO_SPI.h>
#include <GxIO/GxIO.h>

void display_init();
void display_update(uint8_t full_update);
void display_clear();
void display_draw_data(char *temp, char *humidity, char *label, int panel);
void display_wifi_error();
void display_mqtt_error();
#endif
