void comm_setup();
void comm_loop(void *arg);
void comm_set_callback(void (*callback)(char *topic, byte *message, unsigned int length));
void comm_add_subscription(char *topic);
bool comm_get_wifi_connected();
bool comm_get_mqtt_connected();
void comm_publish(char *topic, char *msg);
