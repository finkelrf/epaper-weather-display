#include <Arduino.h>
#include "hal.h"

uint16_t hal_get_battery()
{
  pinMode(GPIO_NUM_35, INPUT);
  return analogRead(GPIO_NUM_35);
}