#include <PubSubClient.h>
#include <WiFi.h>
#include "comm.h"
#include "config.h"

#define MAX_SUBS 10
#define TOPIC_MAX_SIZE 50

const char *ssid = WIFI_SSID;
const char *password = WIFI_PASS;
const char *mqtt_server = MQTT_SERVER;

WiFiClient espClient;
PubSubClient mqtt_client(espClient);

typedef struct
{
  char subs_list[MAX_SUBS][TOPIC_MAX_SIZE];
  int counter = 0;
}subscriptions_t;

subscriptions_t subs;

static void setup_wifi()
{
  int try_counter = 10;
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED && try_counter > 0)
  {
    delay(500);
    Serial.print(".");
    try_counter--;
  }
  Serial.println(WiFi.localIP());
}

static void reconnect_mqtt()
{
  // Loop until we're reconnected
  while (!mqtt_client.connected())
  {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (mqtt_client.connect("ESP8266Client"))
    {
      Serial.println("connected");
      // Subscribe
      for (int i=0; i<subs.counter; i++)
      {
        mqtt_client.subscribe(subs.subs_list[i]);
      }
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(mqtt_client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void comm_add_subscription(char *topic)
{
  strcpy(subs.subs_list[subs.counter++], topic);
}

void comm_set_callback(void (*callback) (char *topic, byte *message, unsigned int length))
{
  mqtt_client.setCallback(callback);
}

void comm_setup()
{
  setup_wifi();
  mqtt_client.setServer(mqtt_server, 1883);
}

void comm_loop(void *arg)
{
  while(1)
  {
    if (!mqtt_client.connected())
    {
      reconnect_mqtt();
    }
    mqtt_client.loop();
  }
}

bool comm_get_wifi_connected()
{
  return WiFi.status() == WL_CONNECTED;
}

bool comm_get_mqtt_connected()
{
  return mqtt_client.connected();
}

void comm_publish(char *topic, char *msg)
{
  mqtt_client.publish(topic, msg);
}