#include "display.h"
#include <Fonts/FreeMonoBold24pt7b.h>
#include <Fonts/FreeMonoBold9pt7b.h>
#include <Fonts/FreeMonoBold12pt7b.h>
#include <Fonts/FreeMonoBold18pt7b.h>
#include <string.h>

GxIO_Class io(SPI,  EPD_CS, EPD_DC,  EPD_RSET);
GxEPD_Class display(io, EPD_RSET, EPD_BUSY);

void display_init(){
    SPI.begin(EPD_SCLK, EPD_MISO, EPD_MOSI);
    display.init(); // enable diagnostic output on Serial
    display.setRotation(3);
    display.setTextColor(GxEPD_BLACK);
    // display.fillScreen(GxEPD_WHITE);
    // display.update();
}

void display_update(uint8_t full_update){
    if (full_update){
        display.update();
    }else{
        display.updateWindow(0, 0, GxEPD_WIDTH, GxEPD_HEIGHT, false);
    }
}

void display_clear(){
    display.fillScreen(GxEPD_WHITE);
}

static void display_draw_temp(int x_offset, int y_offset, char *temp){
    static char integer_part[3];
    static char decimal_part[2];
    char *point = strchr(temp, '.');
    if (point == NULL or decimal_part == NULL){
        // Temperature is not a number with decimal
        return;
    }
    int dist = point - temp; 
    if ( dist < 2)
    {
        // Add white space before number
        integer_part[0] = ' ';
        integer_part[1] = temp[0];
    }
    else
    {
        strncpy(integer_part, temp, 2);
    }
    strncpy(decimal_part, ++point, 1);
    display.setFont(&FreeMonoBold24pt7b);
    display.setCursor(x_offset, y_offset);
    display.print(integer_part);

    // Draw small decimal value
    display.setFont(&FreeMonoBold12pt7b);
    display.setCursor(x_offset+ 57, y_offset-3);
    display.print(decimal_part);

    display.setFont(&FreeMonoBold24pt7b);
    display.setCursor(x_offset+72, y_offset);
    display.print("C");
}

static void display_draw_humidity(int x_offset, int y_offset, char* humidity)
{
    char hum_aux[4];
    strncpy(hum_aux, humidity, 2);
    hum_aux[2] = '%';
    hum_aux[3] = '\0';
    display.setFont(&FreeMonoBold24pt7b);
    display.setCursor(x_offset, y_offset);
    display.print(hum_aux);
}

void display_draw_data(char *temp, char *humidity, char *label, int panel){
    int x_outside_offset = 0;
    if (panel == 1)
        x_outside_offset = 120;
    // Draw separation line
    display.drawLine(x_outside_offset, 0, x_outside_offset, 125, GxEPD_BLACK);

    // Draw outside label
    display.setFont(&FreeMonoBold9pt7b);
    display.setCursor(x_outside_offset+20, 10);
    display.print(label);
    // Draw inside temperature
    display_draw_temp(x_outside_offset+10, 50, temp);
    // Draw inside humidity
    display_draw_humidity(x_outside_offset + 20, 100, humidity);
}

void display_wifi_error()
{
    display_clear();
    display.setFont(&FreeMonoBold18pt7b);
    display.setCursor(20, 70);
    display.print("WiFi Error");
    display_update(true);
}

void display_mqtt_error()
{
    display_clear();
    display.setFont(&FreeMonoBold18pt7b);
    display.setCursor(20, 70);
    display.print("MQTT Error");
    display_update(true);
}
