#include "display.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <freertos/FreeRTOS.h>
#include "freertos/task.h"
#include "driver/gpio.h"
#include "esp_system.h"
#include "comm.h"
#include "hal.h"

#define SLEEP

#define INSIDE_TEMPERATURE_TOPIC "/living_room/temperature"
#define INSIDE_HUMIDITY_TOPIC "/living_room/humidity"
#define OUTSIDE_TEMPERATURE_TOPIC "/outside/temperature"
#define OUTSIDE_HUMIDITY_TOPIC "/outside/humidity"
#define BAT_TOPIC "/weather_display/battery"

#define uS_TO_S_FACTOR 1000000 /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP 60*5        /* Time ESP32 will go to sleep (in seconds) */

typedef struct
{
    char temperature[5];
    char humidity[5];
} weather_data_t;

bool update_display_pending;
bool bat_published;
RTC_DATA_ATTR bool full_update = true;

weather_data_t inside_data;
weather_data_t outside_data;

static void go_to_sleep()
{
    update_display();
    esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
    Serial.println("Setup ESP32 to sleep for every " + String(TIME_TO_SLEEP) +
                   " Seconds");
    Serial.println("Going to sleep now");
    delay(1000);
    Serial.flush();
    esp_deep_sleep_start();
    Serial.println("This will never be printed");
}

static void update_display()
{
    if(!comm_get_wifi_connected())
    {
        display_wifi_error();
        full_update = true;
    }
    else if (!comm_get_mqtt_connected())
    {
        display_mqtt_error();
        full_update = true;
    }
    else if (update_display_pending)
    {
        Serial.println("Updating display");
        display_draw_data(inside_data.temperature, inside_data.humidity, "Inside", 0);
        display_draw_data(outside_data.temperature, outside_data.humidity, "Outside", 1);
        display_update(full_update);
        update_display_pending = false;
        full_update = false;
    }
}

static void send_battery_lvl_task(void *pvParameter)
{
    const TickType_t xDelay = 2000 / portTICK_PERIOD_MS;
    uint16_t bat = 0;
    char bat_str[10];
    for (;;)
    {
        if (!bat_published && comm_get_mqtt_connected())
        {
            bat = hal_get_battery();
            sprintf(bat_str, "%d", bat);
            comm_publish(BAT_TOPIC, bat_str);
        }
        vTaskDelay(xDelay);
    }
}

static void update_display_task(void *pvParameter)
{
    const TickType_t xDelay = 500 / portTICK_PERIOD_MS;
    for (;;){        
        display_clear();
        update_display();
        full_update = false;
        vTaskDelay(xDelay);
    }
}

static void wake_timeout(void *pvParameter)
{
    const TickType_t xDelay = 10000 / portTICK_PERIOD_MS;
    for(;;)
    {
        vTaskDelay(xDelay);
        go_to_sleep();
    }
}

static void copy_msg(char *dest, byte *src, unsigned int length)
{
    memset(dest, 0, length);
    for (int i = 0; i < length; i++)
    {
        dest[i] = (char)src[i];
    }
    dest[length] = '\0';
}

static void mqtt_callback(char *topic, byte *message, unsigned int length)
{
    Serial.print("Message arrived on topic: ");
    Serial.println(topic);

    if (strcmp(topic, INSIDE_TEMPERATURE_TOPIC) == 0)
    {
        copy_msg(inside_data.temperature, message, length);
        Serial.println(inside_data.temperature);
    }
    else if (strcmp(topic, INSIDE_HUMIDITY_TOPIC) == 0)
    {
        copy_msg(inside_data.humidity, message, length);
        Serial.println(inside_data.humidity);
    }
    else if (strcmp(topic, OUTSIDE_TEMPERATURE_TOPIC) == 0)
    {
        copy_msg(outside_data.temperature, message, length);
        Serial.println(outside_data.temperature);
    }
    else if (strcmp(topic, OUTSIDE_HUMIDITY_TOPIC) == 0)
    {
        copy_msg(outside_data.humidity, message, length);
        Serial.println(outside_data.humidity);
    }
    else if (strcmp(topic, BAT_TOPIC) == 0)
    {
        bat_published = true;
    }
    update_display_pending = true;
}

void setup(void)
{
    Serial.begin(115200);
    update_display_pending = false;
    bat_published = false;
    display_init();
    comm_add_subscription(INSIDE_TEMPERATURE_TOPIC);
    comm_add_subscription(INSIDE_HUMIDITY_TOPIC);
    comm_add_subscription(OUTSIDE_TEMPERATURE_TOPIC);
    comm_add_subscription(OUTSIDE_HUMIDITY_TOPIC);
    comm_add_subscription(BAT_TOPIC);
    comm_setup();
    comm_set_callback(mqtt_callback);

    xTaskCreate(&comm_loop, "comm_loop", 2048, NULL, 5, NULL);
    xTaskCreate(&send_battery_lvl_task, "send_battery_lvl_task", 2048, NULL, 4, NULL);
#ifdef SLEEP
    xTaskCreate(&wake_timeout, "wake_timeout", 2048, NULL, 4, NULL);
#else
    delay(3000);
    xTaskCreate(&update_display_task, "update_display_task", 2048, NULL, 5, NULL);
#endif
}


void loop(){

}
